# I2C MPU6050C-INVENSENSE-GY521

MPU-6050

Functions: Accelerometer, Gyroscope, and Temperature.
Connection: I2C

Tested OK on ESP32, Leonardo, Uno and Duino_Zero